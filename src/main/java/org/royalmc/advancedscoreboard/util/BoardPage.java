package org.royalmc.advancedscoreboard.util;

import org.bukkit.entity.Player;


public interface BoardPage
{
    void update(Player p);
}