package org.royalmc.advancedscoreboard.listeners;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.royalmc.advancedscoreboard.Main;

public class BungeeListener implements PluginMessageListener {
	private final Main plugin;

	public BungeeListener(final Main plugin) {
		this.plugin = plugin;
		plugin.getServer().getMessenger().registerIncomingPluginChannel(plugin, "RoyalMC_Economy", this);
		plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "RoyalMC_Economy");
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (!channel.equals("RoyalMC_Economy"))
			return;
		
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));

		String playerName = null;
		long coins = 0;
		long crystals = 0;

		try {
			String subchannel = in.readUTF();
			switch (subchannel) {
			case "EconomyData":
				in.readUTF();
				playerName = in.readUTF();
				coins = in.readLong();
				crystals = in.readLong();
				plugin.coinsData.put(playerName, coins);
				plugin.crystalsData.put(playerName, crystals);
				break;
			case "SetCoins":
				in.readUTF();
				playerName = in.readUTF();
				coins = in.readLong();
				plugin.coinsData.put(playerName, coins);
				break;
			case "ResetCoins":
				in.readUTF();
				playerName = in.readUTF();
				plugin.coinsData.put(playerName, 0l);
				break;
			case "CreditCoins":
				in.readUTF();
				playerName = in.readUTF();
				coins = in.readLong();
				plugin.coinsData.put(playerName, coins);
				break;
			case "DebitCoins":
				in.readUTF();
				playerName = in.readUTF();
				coins = in.readLong();
				if (in.readBoolean()) {
					plugin.coinsData.put(playerName, coins);
				}
				break;
			case "SetCrystals":
				in.readUTF();
				playerName = in.readUTF();
				coins = in.readLong();
				plugin.coinsData.put(playerName, coins);
				break;
			case "ResetCrystals":
				in.readUTF();
				playerName = in.readUTF();
				plugin.coinsData.put(playerName, 0l);
				break;
			case "CreditCrystals":
				in.readUTF();
				playerName = in.readUTF();
				crystals = in.readLong();
				plugin.crystalsData.put(playerName, crystals);
				break;
			case "DebitCrystals":
				in.readUTF();
				playerName = in.readUTF();
				crystals = in.readLong();
				if (in.readBoolean()) {
					plugin.crystalsData.put(playerName, crystals);
				}
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		plugin.displayScoreboard(player);
	}
}