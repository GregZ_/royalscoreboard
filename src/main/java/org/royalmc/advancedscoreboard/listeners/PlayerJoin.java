package org.royalmc.advancedscoreboard.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.royalmc.advancedscoreboard.Main;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class PlayerJoin implements Listener
{
	private final Main plugin;
	
	public PlayerJoin(Main plugin)
	{
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable()
		{
			public void run()
			{
				ByteArrayDataOutput out = ByteStreams.newDataOutput();
				out.writeUTF("EconomyData");
				out.writeUTF("lobby");
				out.writeUTF(p.getUniqueId().toString());
				plugin.getServer().sendPluginMessage(plugin, "RoyalMC_Economy", out.toByteArray());
			}
		}, 2);
	}
}
