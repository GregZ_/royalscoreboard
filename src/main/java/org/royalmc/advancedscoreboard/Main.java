package org.royalmc.advancedscoreboard;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.royalmc.advancedscoreboard.listeners.BungeeListener;
import org.royalmc.advancedscoreboard.listeners.PlayerJoin;
import org.royalmc.advancedscoreboard.tasks.Update;
import org.royalmc.advancedscoreboard.util.ScoreboardUtil;
import org.royalmc.advancedscoreboard.util.Scroller;
import org.royalmc.advancedscoreboard.util.Scroller.ScrollType;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class Main extends JavaPlugin
{
	private String[] lines;

	private final Scroller orangeScroll = new Scroller(ChatColor.WHITE, getConfig().getStringList("Scoreboard.Lines").get(0), "§e", "§6", "§6", true, true, ScrollType.FORWARD);
	private final Scroller purpleScroll = new Scroller(ChatColor.WHITE, getConfig().getStringList("Scoreboard.Lines").get(0), "§d", "§5", "§5", true, true, ScrollType.FORWARD);
	private final Scroller greenScroll = new Scroller(ChatColor.WHITE, getConfig().getStringList("Scoreboard.Lines").get(0), "§a", "§2", "§2", true, true, ScrollType.FORWARD);
	private final Scroller blueScroll = new Scroller(ChatColor.WHITE, getConfig().getStringList("Scoreboard.Lines").get(0), "§3", "§b", "§b", true, true, ScrollType.FORWARD);
	
	public HashMap<String, Long> coinsData;
	public HashMap<String, Long> crystalsData;
	
	private int coinLine;
	private int crystalLine;

	public List<String> configLines;
	
	public void onEnable()
	{
		coinsData = new HashMap<String, Long>();
		crystalsData = new HashMap<String, Long>();
		setupConfig();
		configLines = getConfig().getStringList("Scoreboard.Lines");
		setupLines();
		new Update(this);
		new PlayerJoin(this);
		new BungeeListener(this);
		
		Bukkit.getOnlinePlayers().stream().forEach(p -> requestData(p));
	}

	private void requestData(Player p) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("EconomyData");
		out.writeUTF("lobby");
		out.writeUTF(p.getUniqueId().toString());
		getServer().sendPluginMessage(this, "RoyalMC_Economy", out.toByteArray());
	}

	private void setupConfig()
	{
		if (!new File("plugins/AdvancedScoreboard/config.yml").exists())
		{
			getConfig().getDefaults();
			getConfig().options().copyDefaults(true);
			getConfig().options().header("Update intervals measured in ticks. 20 ticks = 1 second.");
			saveConfig();
		}
	}

	private void setupLines()
	{
		lines = new String[configLines.size()];

		for (int i = 0; i < lines.length; i++) 
		{		
			if(configLines.get(i).contains("%crystals%"))
				crystalLine = i;
			else if(configLines.get(i).contains("%coins%"))
				coinLine = i;
			
			if (configLines.get(i).equalsIgnoreCase("BLANK")) 
				lines[i] = ChatColor.values()[i] + "";
			
			else if (Arrays.asList(lines).contains(ChatColor.translateAlternateColorCodes('&', configLines.get(i)))) 
				lines[i] = ChatColor.translateAlternateColorCodes('&', configLines.get(i)) + ChatColor.values()[i];

			else lines[i] = ChatColor.translateAlternateColorCodes('&', configLines.get(i));
		}
	}
	
	private void updateLines(Player p)
	{
		for (int i = 0; i < lines.length; i++) 
		{						
			if(configLines.get(i).equalsIgnoreCase("%crystals%") && crystalsData.get(p.getName()) != null)
				lines[i] = lines[i].replace(lines[crystalLine], crystalsData.get(p.getName()) + "");
			
			else if(configLines.get(i).equalsIgnoreCase("%coins%") && coinsData.get(p.getName()) != null)
				lines[i] = lines[i].replace(lines[coinLine], coinsData.get(p.getName()) + "");
			
			lines[i] = ChatColor.translateAlternateColorCodes('&', lines[i]);
		}
	}

	public String[] getLines()
	{
		return lines;
	}

	public Scroller[] getScrollers()
	{
		return new Scroller[] { orangeScroll, purpleScroll, greenScroll, blueScroll };
	}

	public void displayScoreboard(Player p)
	{
		updateLines(p);
		ScoreboardUtil.unrankedSidebarDisplay(p, lines);
	}
}