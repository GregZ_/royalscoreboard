package org.royalmc.advancedscoreboard.tasks;

import org.bukkit.entity.Player;
import org.royalmc.advancedscoreboard.Main;
import org.royalmc.advancedscoreboard.util.Scroller;

public class Update implements Runnable
{
	private final Main plugin;
	private final Scroller[] scrollers;
	private int i = 0;

	public Update(Main plugin)
	{
		this.plugin = plugin;
		this.scrollers = plugin.getScrollers();
		plugin.getServer().getScheduler().runTaskTimer(plugin, this, 0L, plugin.getConfig().getInt("General.Update"));
	}

	public void run()
	{
		for (Player p : this.plugin.getServer().getOnlinePlayers()) 
		{
			this.plugin.displayScoreboard(p);
		}
		Scroller activeScroll = this.scrollers[this.i];

		if (activeScroll.getPosition() >= activeScroll.getString().length())
		{
			if (this.i == 3) 
				this.i = 0;

			this.i += 1;
			activeScroll = this.scrollers[this.i];
		}
		this.plugin.getLines()[0] = activeScroll.next();
	}
}
